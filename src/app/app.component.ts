import { Component } from '@angular/core';
import { TestDataService } from './test-data.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css',
  './../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class AppComponent {
  title = 'turism';
  data: Object = {};
  constructor(private test: TestDataService) {}

  ngOnInit() {
    this.getData();
  }
  exampleForm = new FormGroup ({
    firstName: new FormControl(),
    lastName: new FormControl()
  });

  getData(): void{
    this.test.getData()
    .subscribe(data => this.data = data
     );
    console.log('***********1');
  }
  onSubmit() {
    // stop here if form is invalid
    if (!this.exampleForm.invalid) {
      console.log(this.exampleForm.value);
      this.test.setData(this.exampleForm.value)
      .subscribe(person => console.log(person));
    }
  }  
}
