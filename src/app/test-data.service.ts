import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestDataService {

  private fakeUrl = environment.fakeServer + 'person';
  constructor(
    private http: HttpClient) { }

  getData(): Observable<Object> {
    return this.http.get<String[]>(this.fakeUrl);
  }
  
  setData(data: Object): Observable<String[]> {
    return this.http.post<String[]>(this.fakeUrl, data);
  }
}
